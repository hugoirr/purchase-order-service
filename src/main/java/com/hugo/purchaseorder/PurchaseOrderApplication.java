package com.hugo.purchaseorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class PurchaseOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(PurchaseOrderApplication.class, args);
    }

}
