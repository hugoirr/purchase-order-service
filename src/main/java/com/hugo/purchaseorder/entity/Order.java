package com.hugo.purchaseorder.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Document(collection = "order")
@Setter
@Getter
public class Order {

    @MongoId(value = FieldType.OBJECT_ID)
    private Object id;

    private String product;

    private int total;

    private String customer;

    public Order(String product, int total, String customer) {
        this.product = product;
        this.total = total;
        this.customer = customer;
    }
    public Order() {

    }
}
