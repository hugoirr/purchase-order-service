package com.hugo.purchaseorder.repository;

import com.hugo.purchaseorder.entity.Order;
import com.hugo.purchaseorder.service.NextSequenceService;
import com.mongodb.client.model.MapReduceAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.mapreduce.MapReduceOptions;
import org.springframework.data.mongodb.core.mapreduce.MapReduceResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;


import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@Repository
public class OrderDAO {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private NextSequenceService sequenceService;

    public String saveOrder(Order order) {
        if (order != null) {
            order.setId( sequenceService.getNextSequence("customSequences") );
            mongoTemplate.save(order);
            return "Success to order. The order id is " + order.getId();
        }
        return "Failed to order";
    }

    public Order getOrder(Long id) {
        Order order = mongoTemplate.findById(id, Order.class);
        if (order != null) {
            return order;
        } else {
            return null;
        }
    }

    public List<Order> getAllOrder() {
        return mongoTemplate.findAll(Order.class);
    }

    public List<Object> getSumOfTotalEachCustomer() {
        MatchOperation filterCustomer = match(new Criteria(""));
        GroupOperation groupByCustomer = group("customer")
                .sum("total").as("total");
        SortOperation sortByCustomerDesc = sort(Sort.by(Sort.Direction.DESC, "total"));

        Aggregation aggregation = newAggregation(
                groupByCustomer, filterCustomer, sortByCustomerDesc);

        AggregationResults<Object> result = mongoTemplate.aggregate(
                aggregation, "order", Object.class);
        System.out.println(result.getRawResults());


        System.out.println(result.getMappedResults());
        return result.getMappedResults();
    }

    public void countCustomerOrder() {

        String mapFunc = "function() {emit(this.customer, 1); }";

        String reduceFunc = "function(customer, count) { return Array.sum(count) }";

        MapReduceResults<Order> result = mongoTemplate.mapReduce("order", mapFunc, reduceFunc, new MapReduceOptions().outputCollection("out_customer"), Order.class);
        System.out.println(result.getOutputCollection());
        System.out.println(result.getRawResults());
        System.out.println(result.getClass());
//        return result.getRawResults();

    }



}
