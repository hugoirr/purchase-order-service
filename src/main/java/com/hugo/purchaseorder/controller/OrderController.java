package com.hugo.purchaseorder.controller;

import com.hugo.purchaseorder.entity.Order;
import com.hugo.purchaseorder.repository.OrderDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderDAO orderDAO;

    @PostMapping("/post")
    public String sendOrder(@RequestBody Order order ) {
        return orderDAO.saveOrder(order);
    }

    @GetMapping("/get/{id}")
    public Order getOrder(@PathVariable Long id) {
        return orderDAO.getOrder(id);
    }

    @GetMapping("/get/all")
    public List<Order> getOrders() {
        return orderDAO.getAllOrder();
    }

    @GetMapping("/get/sumtotal")
    public List<Object> getSumTotal() {
        return orderDAO.getSumOfTotalEachCustomer();
    }

    @GetMapping("/get/countorder")
    public void countCustomerOrder() {
        orderDAO.countCustomerOrder();
    }
}
