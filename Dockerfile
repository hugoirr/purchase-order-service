FROM adoptopenjdk/openjdk11:alpine-jre

ENV PORT 8080

ARG JAR_FILE=target/*.jar

COPY ${JAR_FILE} app.jar

# java -jar /opt/app/app.jar
ENTRYPOINT ["java","-jar","app.jar"]